pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;

contract Marketplace {
  Realty[] public realties;
  uint public lastId = 0;

  mapping (address => uint) ownerRealtyCount;
  mapping (uint => Realty) idRealty;

  event RealtyUpdated(uint id, uint price, bool isSold, string imageUrl, string postalAddress, string name, string description, address owner);

  constructor() public {}

  modifier requireOwnership(uint id) {
    require(msg.sender == idRealty[id].owner);
    _;
  }

  modifier requireNotOwnership(uint id) {
    require(msg.sender != idRealty[id].owner);
    _;
  }

  modifier requireRealtyExists(uint id) {
    require(idRealty[id].exists);
    _;
  }

  modifier isNotSold(uint id) {
    require(!idRealty[id].isSold);
    _;
  }

  function deleteRealty(uint id) external requireOwnership(id) requireRealtyExists(id) {
    uint index = 0;

    for (uint i = 0; i < realties.length; i++){
      if (realties[i].id == id) {
        index = i;
        break;
      }
    }

    for (uint i = index; i < realties.length - 1; i++){
      realties[i] = realties[i+1];
    }

    realties.length--;
    delete idRealty[id];
    ownerRealtyCount[msg.sender]--;
  }

  function sellRealty(uint price, string calldata imageUrl, string calldata postalAddress, string calldata name, string calldata description) external {
    uint id = lastId;

    idRealty[id] = Realty(id, price, false, true, imageUrl, postalAddress, name, description, msg.sender);
    realties.push(Realty(id, price, false, true, imageUrl, postalAddress, name, description, msg.sender));
    ownerRealtyCount[msg.sender]++;
    lastId++;

    emit RealtyUpdated(id, price, false, imageUrl, postalAddress, name, description, msg.sender);
  }

  function buyRealty(uint id) external requireNotOwnership(id) requireRealtyExists(id) isNotSold(id) {
    for (uint i = 0; i < realties.length; i++) {
      if (realties[i].id == id) {
        realties[i].isSold = true;
        break;
      }
    }

    idRealty[id].isSold = true;
    emit RealtyUpdated(idRealty[id].id, idRealty[id].price, idRealty[id].isSold, idRealty[id].imageUrl, idRealty[id].postalAddress, idRealty[id].name, idRealty[id].description, idRealty[id].owner);
  }

  function getRealty(uint id) external view returns(uint, uint, bool, string memory, string memory, string memory, string memory, address) {
    uint realtyId = id;
    return (idRealty[realtyId].id, idRealty[realtyId].price, idRealty[realtyId].isSold, idRealty[realtyId].imageUrl, idRealty[realtyId].postalAddress, idRealty[realtyId].name, idRealty[realtyId].description, idRealty[realtyId].owner);
  }

  function getRealties() external view returns(uint[] memory, uint[] memory, bool[] memory, string[] memory, string[] memory, string[] memory, string[] memory, address[] memory) {
    uint[] memory ids = new uint[](realties.length);
    uint[] memory prices = new uint[](realties.length);
    bool[] memory areSold = new bool[](realties.length);
    string[] memory imageUrls = new string[](realties.length);
    string[] memory postalAddresses = new string[](realties.length);
    string[] memory names = new string[](realties.length);
    string[] memory descriptions = new string[](realties.length);
    address[] memory owners = new address[](realties.length);

    for(uint i = 0; i < realties.length; i++) {
      ids[i] = realties[i].id;
      prices[i] = realties[i].price;
      areSold[i] = realties[i].isSold;
      imageUrls[i] = realties[i].imageUrl;
      postalAddresses[i] = realties[i].postalAddress;
      names[i] = realties[i].name;
      descriptions[i] = realties[i].description;
      owners[i] = realties[i].owner;
    }

    return (ids, prices, areSold, imageUrls, postalAddresses, names, descriptions, owners);
  }

  function getRealtiesByOwner(address owner) external view returns(uint[] memory, uint[] memory, bool[] memory, string[] memory, string[] memory, string[] memory, string[] memory) {
    address ownerAddr = owner;
    uint realtyCount = ownerRealtyCount[ownerAddr];

    uint[] memory ids = new uint[](realtyCount);
    uint[] memory prices = new uint[](realtyCount);
    bool[] memory areSold = new bool[](realtyCount);
    string[] memory imageUrls = new string[](realtyCount);
    string[] memory postalAddresses = new string[](realtyCount);
    string[] memory names = new string[](realtyCount);
    string[] memory descriptions = new string[](realtyCount);

    for(uint i = 0; i < realties.length; i++) {
      if (ownerAddr == realties[i].owner) {
        ids[i] = realties[i].id;
        prices[i] = realties[i].price;
        areSold[i] = realties[i].isSold;
        imageUrls[i] = realties[i].imageUrl;
        postalAddresses[i] = realties[i].postalAddress;
        names[i] = realties[i].name;
        descriptions[i] = realties[i].description;
      }
    }

    return (ids, prices, areSold, imageUrls, postalAddresses, names, descriptions);
  }

  struct Realty {
    uint id;
    uint price;
    bool isSold;
    bool exists;
    string imageUrl;
    string postalAddress;
    string name;
    string description;
    address owner;
  }
}