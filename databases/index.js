const contract = require('truffle-contract');
const web3 = require('./web3');

const marketplace_artifact = require('../build/contracts/Marketplace.json');
const Marketplace = contract(marketplace_artifact);
Marketplace.setProvider(web3.currentProvider);

module.exports = {
    Marketplace,
    web3
};