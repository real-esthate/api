const settings = require('../truffle-config').networks.development;
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider(`http://${settings.host}:${settings.port}`));

module.exports = web3;