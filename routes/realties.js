const router = require('express').Router();
const actions = require('../actions').marketplace.realties;

router.get('/', actions.getAll);
router.post('/', actions.create);

router.get('/:id', actions.getOne);
router.put('/:id', actions.update);
router.delete('/:id', actions.remove);
router.post('/:id/buy', actions.buy);

module.exports = router;
