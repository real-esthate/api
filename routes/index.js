const router = require('express').Router();
const bodyParser = require('body-parser');
const cors = require('cors');

router.use(cors());
router.use(bodyParser.json());
router.use('/realties', require('./realties'));
router.use('/accounts', require('./accounts'));
router.use(require('./404'));

module.exports = router;
