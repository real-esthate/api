const router = require('express').Router();
const actions = require('../actions').marketplace.accounts;

router.get('/', actions.getAll);
router.get('/:id', actions.getOne);

module.exports = router;