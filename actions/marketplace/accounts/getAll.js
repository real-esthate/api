const web3 = require('../../../databases').web3;

const getAll = async (req, res) => {
  web3.eth.getAccounts((err, accounts) => {
    if (err != null) {
      return res.status(500).send('There was an error retrieving the accounts');
    }

    return res.send(accounts);
  });
};

module.exports = getAll;
