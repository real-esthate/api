const databases = require('../../../databases');

const getOne = async (req, res) => {
  if (!req.params.id) {
    return res.status(400).send('Missing parameter "id"');
  }

  const owner = req.params.id;

  const balance = await databases.web3.eth.getBalance(owner);
  const balanceDecimal = databases.web3.toDecimal(balance);
  const instance = await databases.Marketplace.deployed();

  const realties = [];
  const realtiesSplit = await instance.getRealtiesByOwner(owner, { from: owner });

  if (realtiesSplit['0'] && realtiesSplit['0'].length > 0) {
    for(let i = 0; i < realtiesSplit['0'].length; i++) {
      realties.push({
        id: realtiesSplit['0'][i],
        price: databases.web3.toDecimal(realtiesSplit['1'][i]),
        isSold: realtiesSplit['2'][i],
        imageUrl: realtiesSplit['3'][i],
        postalAddress: realtiesSplit['4'][i],
        name: realtiesSplit['5'][i],
        description: realtiesSplit['6'][i]
      })
    }
  }

  const account = {
    address: req.params.id,
    balance: balanceDecimal,
    realties: realties
  };

  return res.send(account);
};

module.exports = getOne;
