const databases = require('../../../databases');

const buy = async (req, res) => {
  if (!req.headers.address) {
    return res.sendStatus(401);
  }

  const instance = await databases.Marketplace.deployed();
  const realtySplit = await instance.getRealty(req.params.id);
  const realty = {
    id: realtySplit['0'],
    price: databases.web3.toDecimal(realtySplit['1']),
    isSold: realtySplit['2'],
    imageUrl: realtySplit['3'],
    postalAddress: realtySplit['4'],
    name: realtySplit['5'],
    description: realtySplit['6'],
    owner: realtySplit['7']
  };

  if (realty.isSold) {
    return res.status(404).send('Already sold');
  }

  databases.web3.eth.sendTransaction({
    from: req.headers.address,
    to: realty.owner,
    value: realty.price,
  }, async function(err, transactionHash) {
    if (err) {
      return res.status(402).send();
    } else {
      await instance.buyRealty(req.params.id, { from: req.headers.address });
      return res.send({ hash: transactionHash });
    }
  });
};

module.exports = buy;
