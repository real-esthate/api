const databases = require('../../../databases');

const getAll = async (req, res) => {
  const instance = await databases.Marketplace.deployed();
  const realties = [];
  const realtiesSplit = await instance.getRealties();

  if (realtiesSplit['0'] && realtiesSplit['0'].length > 0) {
    for(let i = 0; i < realtiesSplit['0'].length; i++) {
      realties.push({
        id: realtiesSplit['0'][i],
        price: databases.web3.toDecimal(realtiesSplit['1'][i]),
        isSold: realtiesSplit['2'][i],
        imageUrl: realtiesSplit['3'][i],
        postalAddress: realtiesSplit['4'][i],
        name: realtiesSplit['5'][i],
        description: realtiesSplit['6'][i],
        owner: realtiesSplit['7'][i]
      })
    }
  }

  return res.send(realties);
};

module.exports = getAll;
