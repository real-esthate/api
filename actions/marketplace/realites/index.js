const buy = require('./buy');
const create = require('./create');
const getAll = require('./getAll');
const getOne = require('./getOne');
const remove = require('./remove');
const update = require('./update');

module.exports = {
    buy,
    getAll,
    getOne,
    create,
    remove,
    update
};
