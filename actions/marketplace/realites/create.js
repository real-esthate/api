const databases = require('../../../databases');

const create = async (req, res) => {
  if (!req.headers.address || !req.body || !req.body.price || !req.body.imageUrl || !req.body.postalAddress || !req.body.name || !req.body.description) {
    return res.status(400).send('Missing parameters');
  }

  const price = req.body.price;
  const imageUrl = req.body.imageUrl;
  const postalAddress = req.body.postalAddress;
  const name = req.body.name;
  const description = req.body.description;

  const instance = await databases.Marketplace.deployed();
  await instance.sellRealty(price, imageUrl, postalAddress, name, description, { from: req.headers.address });

  return res.sendStatus(201);
};

module.exports = create;
