const databases = require('../../../databases');

const getOne = async (req, res) => {
  if (!req.params.id) {
    return res.status(400).send('Missing param id');
  }

  const instance = await databases.Marketplace.deployed();
  const realtySplit = await instance.getRealty(req.params.id);
  const realty = {
    id: realtySplit['0'],
    price: databases.web3.toDecimal(realtySplit['1']),
    isSold: realtySplit['2'],
    imageUrl: realtySplit['3'],
    postalAddress: realtySplit['4'],
    name: realtySplit['5'],
    description: realtySplit['6'],
    owner: realtySplit['7']
  };

  return res.send(realty);
};

module.exports = getOne;
