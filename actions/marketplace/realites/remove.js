const databases = require('../../../databases');

const remove = async (req, res) => {
  if (!req.params.id && !req.headers.address) {
    return res.status(400).send('Missing parameter id');
  }

  const instance = await databases.Marketplace.deployed();
  await instance.deleteRealty(req.params.id, {from: req.headers.address});

  return res.sendStatus(204);
};

module.exports = remove;
