const realties = require('./realites');
const accounts = require('./accounts');

module.exports = {
  realties,
  accounts
};
