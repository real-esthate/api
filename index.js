const app = require('express')();

app.settings = require('./settings');
app.databases = require('./databases');
app.routes = require('./routes');
app.actions = require('./actions');

app.use(app.routes);
app.listen(app.settings.port, () => console.log(`Marketplace API listening on port ${app.settings.port}`));
